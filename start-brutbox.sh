#!/bin/bash
# Launch script (Linux)

# Project name
PROJECT="BRUTBOX"

# Get absolute path
DIR="$(cd "$(dirname "$0")" && pwd)"

# Check if tcl tk are installed, if not it will be installed
[[ $(dpkg-query --show --showformat='${db:Status-Status}' 'tcl' 'tk') != installedinstalled ]] && sudo apt-get -yq --fix-missing install tcl tk

# Open Pure Data and settings
$DIR/pd/bin/pd -midiindev 1 -helppath $DIR/pd/doc/5.reference -font-weight normal -open $DIR/malinette-ide/$PROJECT.pd -path $DIR/pd/extra -path $DIR/externals -path $DIR/malinette-ide/abs/brutbox -path $DIR/malinette-ide/abs/malinette-abs -path $DIR/externals/bassemu~ -path $DIR/externals/comport -path $DIR/externals/creb -path $DIR/externals/cyclone -path $DIR/externals/ext13 -path $DIR/externals/ggee -path $DIR/externals/hcs -path $DIR/externals/iemguts -path $DIR/externals/pduino -path $DIR/externals/iemlib -path $DIR/externals/list-abs -path $DIR/externals/mapping -path $DIR/externals/maxlib -path $DIR/externals/moocow  -path $DIR/externals/pix_fiducialtrack -path $DIR/externals/pmpd -path $DIR/externals/puremapping -path $DIR/externals/purepd -path $DIR/externals/sigpack -path $DIR/externals/tof -path $DIR/externals/zexy -path $DIR/externals/hid -path $DIR/externals/completion-plugin -lib zexy -lib iemlib
