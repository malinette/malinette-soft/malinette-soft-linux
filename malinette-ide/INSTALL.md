# INSTALLATION

To use the Malinette on your computer, you have to install Pure Data (>0.51) and externals. It's not very easy, that's why we propose the [malinette-soft](https://framagit.org/malinette/malinette-soft) version. But if you want a rich experience with Pure Data, it is better to install it on your computer. Note that, since version 0.46, you can download the externals one by one from the Pure Data menu "Help > Find externals".

## Summary
- Install and run Pure Data.
- Copy externals from malinette-soft to your Pd/externals folder.
- Download malinette-ide
- Give the paths of all externals and the malinette to Pd with the .pdsettings file.

## Windows
- Download and install [Pure Data.exe](http://msp.ucsd.edu/Software/pd-0.51-3.windows-installer.exe).
- Install Pure Data and run it. It should create a "%AppData%\Pd\externals" folder (usually C:\Users\user_name\AppData\Roaming\Pd), where we want to put Malinette files. Quit it.
- Download [malinette-soft-win-master.zip](https://framagit.org/malinette/malinette-soft/malinette-soft-win/-/archive/master/malinette-soft-win-master.zip) to get all externals and the "msvcr71.dll" file. Command to find it in tour computer : dir msvcr71.dll /s /p
- Extract  malinette-soft-win-master. Move all files in "externals" to your personal "%AppData%\Pd\externals" folder.
- Copy the "malinette-soft-win-master/pd/bin/msvcr71.dll" file to the "pd/bin" folder or to "C:\Windows\System Folder". [Source](https://lists.puredata.info/pipermail/pd-list/2017-04/118639.html). Needed when using Gem library.
- Download the most recent release of [malinette-ide.zip](https://framagit.org/malinette/malinette-ide/-/archive/master/malinette-ide-master.zip), and extract it to "%AppData%\Pd\" without the word "master" it is easier. **Be aware of keeping your  old "malinette-ide" folder if you got one by renaming it**.
- Now, Pd just want to know the paths for all these new externals. You can add them one by one with the "Pd > preferences" menu or by load a .pdsettings file. To do this, open "malinette-ide/preferences/malinette-win.pdsettings" with a text editor and replace "<PD-BIN>" by the Pd path and "<PD-DOCS>" by the "%AppData%\Pd\". Save it.
- Run Pd and load paths with "Pd > preferences > load from" and find the new saved "malinette-win.pdsettings" file.
- Open "malinette-ide/abs/malinette-abs/0-MANUAL.pd" to see if you can load all externals.
- Open "malinette-ide/MALINETTE.pd" to start patching.

**Some issues** : 
- Force [cyclone/gate] instead of [iemlib1/gate]. [source](https://lists.puredata.info/pipermail/pd-list/2017-12/121017.html)
- [pduino] on Windows 10 does not work with an Arduino Leonardo and Firmata firmware. Solutions: use a UNO or pass the Leonardo to MIDI. See [malinette-hw/midi-box](https://framagit.org/malinette/malinette-hw/tree/master/midi-box) to do it.
- [pix_video] on Windows 10 does not work with the integrated camera. May be this version of Gem on : https://github.com/avilleret/Gem/releases or use an external usb webcam (~logitech)

## Mac OS X
- Download and install [Pure Data.tar.gz](http://msp.ucsd.edu/Software/pd-0.51-3.mac.tar.gz) : 32-bit ("i386") Macintosh version for OSX 10.6 or later. We use the Gem library, so you should download the 32 bits version. Some infos on the [pd-list](https://lists.puredata.info/pipermail/pd-list/2017-09/120361.html).
- May be you should need to install [X Quartz](http://xquartz.macosforge.org/landing) too. Not sure.
- Run Pure Data. You should pass through the "Impossible to open Pd.app" window. The common way is to right click on "Pd.app" and open it. Or you could also go to "System Preferences > Security and confidentiality" and allow "Pd.app". When Pd is opened, it should create a "~/Library/Pd/externals" folder, where we want to put Malinette files. To view this folder, clic on "Finder > Go to" with the "alt" Key...
- Download [malinette-soft-mac-master.zip](https://framagit.org/malinette/malinette-soft/malinette-soft-mac/-/archive/master/malinette-soft-mac-master.zip) to get all externals and the "msvcr71.dll" file.
- Extract  malinette-soft-mac-master. Move all files in "externals" to your personal "%AppData%\Pd\externals" folder.
- Download the most recent release of [malinette-ide.zip](https://framagit.org/malinette/malinette-ide/-/archive/master/malinette-ide-master.zip), and extract it to "%AppData%\Pd\" without the word "master" it is easier. **Be aware of keeping your  old "malinette-ide" folder if you got one by renaming it**.
- Now, Pd just want to know the paths for all these new externals. You can add them one by one with the "Pd > preferences" menu or by load the .pdsettings file. To do this, open "malinette-ide/preferences/malinette.pdsettings" with a text editor and replace "<PD-BIN>" by the Pd path and "<PD-DOCS>" by the "%AppData%\Pd\". Save it.
- Run Pd and load paths with "Pd > preferences > load from" and find the new saved "malinette.pdsettings" file.
- Open "malinette-ide/abs/malinette-abs/0-MANUAL.pd" to see if you can load all externals.
- Open "malinette-ide/MALINETTE.pd" to start patching.

**Issues** : 
- Using [pix_video] on integrated camera. May be this version of Gem on : https://github.com/avilleret/Gem/releases and webcam usb logitech
- Duplicate key binding events with non-US keyboard locale on macOS Tk 8.5+. [Source](https://github.com/pure-data/pure-data/issues/239)


## Raspberry PI 2 and 3
- Download and install [Pure Data armf.deb](https://deb.debian.org/debian/pool/main/p/puredata/)
- sudo apt-get install tcl tk libftgl2
- Follow Linux instructions below.


## Linux
(Option: sudo apt-get install puredata)

### Build Pure Data
See "INSTALL.txt" of Pure Data for advanced setup.

- sudo apt-get update
- sudo apt-get upgrade
- sudo apt-get install build-essential automake autoconf libtool gettext libasound2-dev tcl tk libftgl2 libjack-jackd2-dev
- git clone https://github.com/pure-data/pure-data
- ./autogen.sh
- ./configure --enable-jack
- make
- sudo make install

### Get compiled Pure Data
- Alternative is to install a .deb package on [debian/pool](https://deb.debian.org/debian/pool/main/p/puredata/)

### Next
- Run Pure Data. It should create a "~/Documents/Pd/externals" folder, where we want to put Malinette files.
- Download [malinette-soft-linux-master.zip](https://framagit.org/malinette/malinette-soft/malinette-soft-linux/-/archive/master/malinette-soft-linux-master.zip) to get all externals.
- Extract  malinette-soft-linux-master. Move all files in "externals" to your personal "~/Documents/Pd/externals/" folder.
- Download the most recent release of [malinette-ide.zip](https://framagit.org/malinette/malinette-ide/-/archive/master/malinette-ide-master.zip), and extract it to "~/Documents/Pd" without the word "master" it is easier. **Be aware of keeping your old "malinette-ide" folder if you got one by renaming it**.
- Now, Pd just want to know the paths for all these new externals. You can add them one by one with the "Pd > preferences" menu or by load the .pdsettings file. To do this, open "malinette-ide/preferences/malinette.pdsettings" with a text editor and replace (search and replace) "<PD-BIN>" by the Pd path ("/usr/local/lib/pd") and "<PD-DOCS>" by the "/home/<USER>/Documents/Pd", where <USER> is your login. Save it.
- Run Pd and load paths with "Pd > preferences > load from" and find the new saved "malinette.pdsettings" file.
- Open "malinette-ide/abs/malinette-abs/0-MANUAL.pd" to see if you can load all externals.
- Open "malinette-ide/MALINETTE.pd" to start patching.
- For **Gem**, it could be needed to install it with "apt-get install" instead of the "Pd > Find externals" way. The simpliest way : sudo apt install libftgl2

## Startup command
	pd -font-weight normal -open ~/Documents/Pd/malinette-ide/MALINETTE.pd

The "-font-weight normal" avoid bold font (issue ?).
