#! /bin/bash
# Rename abstractions into files
# Usage : bash rename_abstraction.sh folder/abstraction_old folder/abstraction_new
# Example : bash rename_abstraction.sh malinette-abs/video-in malinette-abs/video-camera

# Get filenames
file_old=$(basename "$1")
file_new=$(basename "$2")

# Change the name of the file
mv "../abs/$1.pd" "../abs/$2.pd"
mv "../abs/$1-help.pd" "../abs/$2-help.pd"

# Change the name into all other files (patchs) : abstractions, examples, projects
pattern="../*/*/*.pd"

# Change pattern to get filenames with spaces
IFS="
"
for file in $(ls -R -d $pattern) ; do
    sed -i -e "s/^\(#X obj [0-9]\+ [0-9]\+\) $file_old\([ ;]\)/\1 $file_new\2/" $file
done


# Todo : change the title in the help patch + change the name in the default list
