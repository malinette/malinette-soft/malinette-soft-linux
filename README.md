# malinette-soft
Open Source Kit For Programming Interactivity. 

## Description
Standalone version of the [malinette](http://malinette.info) project. It contains Pure Data, 21 libraries, malinette-ide and a startup script. To stay up-to-date, you can replace the [malinette-ide](https://framagit.org/malinette/malinette-ide) folder.

## Installation
- Download
- Extract
- Launch "start-malinette.sh" script. Right Click > Open With > Other Application ... > Terminal

On Linux, you will see something like "Do you want to launch MALINETTE.pd or display its contents". Choose "launch".

## Launcher
You can add an alias or a launcher to make easiest the way to launch La Malinette. On Linux systems, you can add a launcher to the menu bar, edit it, with an image and a command line "~/malinette-soft-linux/start-malinette.sh". Some documentations in french on [ubuntu.fr](https://doc.ubuntu-fr.org/raccourci-lanceur).

## Arduino
Sometimes, you need to type "sudo usermod -a -G USER NAME dialout" to add user into dialout group. Log out and log back in.